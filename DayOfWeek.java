import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int d, m, y;
        System.out.print("Podaj dzień (1-31): ");
        d = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Podaj miesiąc (1-12): ");
        m = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Podaj rok: ");
        y = scanner.nextInt();
        scanner.nextLine();

        int y0 = y - (14 - m) / 12;
        int x = y0 + y0 / 4 - y0 / 100 + y0 / 400;
        int m0 = m + 12 * ((14 - m) / 12) - 2;
        int d0 = (d + x + 31 * m0 / 12) % 7;

        System.out.print(d + "-" + m + "-" + y + " to ");

        switch (d0) {
            case 0:
                System.out.println("niedziela");
                break;
            case 1:
                System.out.println("poniedziałek");
                break;
            case 2:
                System.out.println("wtorek");
                break;
            case 3:
                System.out.println("środa");
                break;
            case 4:
                System.out.println("czwartek");
                break;
            case 5:
                System.out.println("piątek");
                break;
            case 6:
                System.out.println("sobota");
                break;
        }
    }
}