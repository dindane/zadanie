public class Runner {
    public static void main(String[] args) {
        Account a1 = new Account("1234", "Bob", 1000);
        Account a2 = new Account("5678", "Tim", 2000);

        a1.credit(100);
        a2.debit(200);

        System.out.println(a1);
        System.out.println(a2);

        System.out.println("Przelewam 500 z a1 do a2:");
        a1.transferTo(a2, 500);

        System.out.println(a1);
        System.out.println(a2);

        System.out.println();

        Author author = new Author("Roman Pisarski", "roman@wp.pl", 'm');
        Book book1 = new Book("O psie, który jeździł koleją", author, 19.99);
        System.out.println(book1);
    }
}
